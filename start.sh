#!/bin/bash

cp .env.example .env

docker network create -d bridge nginx-proxy

docker-compose up -d
